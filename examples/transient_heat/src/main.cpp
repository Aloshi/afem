#include <afem.h>

#ifndef M_PI
#    define M_PI 3.14159265358979323846
#endif

#include <vector>
#include <petsc/ksp.h>
#include <common/log.h>

struct HTNodeData {
  PetscScalar u;
  PetscScalar u_analytical;

  static const std::vector<NodeVariable> variables;
};

const std::vector<NodeVariable> HTNodeData::variables = {
	NodeVariable("u", offsetof(HTNodeData, u)),
	NodeVariable("u_analytical", offsetof(HTNodeData, u_analytical)),
};

class HTAnalyticSol {
 public:
  HTAnalyticSol(int nsd) : nsd_(nsd) {}

  inline double calc_at(const Vector3& pos, double t) {
    if (nsd_ == 1)
      return sin(M_PI * pos.x()) * exp(-t);
    else if (nsd_ == 2)
      return sin(M_PI * pos.x()) * sin(M_PI * pos.y()) * exp(-t);
    else if (nsd_ == 3)
      return sin(M_PI * pos.x()) * sin(M_PI * pos.y()) * sin(M_PI * pos.z()) * exp(-t);

    throw NotImplementedException();
  }

  inline int nsd() const {
    return nsd_;
  }

 private:
  int nsd_;
};

class HTEquation : public Equation<HTNodeData> {
 public:
  HTEquation(std::shared_ptr<HTAnalyticSol> analytic_sol, double dt)
    : analytic_sol_(analytic_sol), dt_(dt) {
    k_ = 1.0 / (analytic_sol_->nsd() * M_PI * M_PI);  // diffusivity constant
  }

  std::vector<std::string> solve_for() const {
    return { "u" };
  }

  void integrands_matrix(FEMElm& fe, ElemMatrix& ae, MeshDataT& data) override {
    //Eigen::MatrixXd M = fe.N() * fe.N().transpose() * fe.jacc_x_w() / dt_;
    //ae += fe.dN() * fe.dN().transpose() * fe.jacc_x_w() * diffusivity + M;

    for (int a = 0; a < fe.nbf(); a++) {
      for (int b = 0; b < fe.nbf(); b++) {
        double M = fe.N(a) * fe.N(b) * fe.jacc_x_w();
        double N = 0;
        for (int k = 0; k < fe.nsd(); k++) {
          N += k_ * fe.dN(a, k) * fe.dN(b, k) * fe.jacc_x_w();
        }
        // Add term to the A element matrix
        ae(a, b) += M / dt_ + N;
      }
    }
  }

  void integrands_vector(FEMElm& fe, ElemVector& be, MeshDataT& data) override {
    double u_pre = fe.variable(data, offsetof(HTNodeData, u));
    be += fe.N() * u_pre * fe.jacc_x_w() / dt_;
  }

 private:
  std::shared_ptr<HTAnalyticSol> analytic_sol_;
  double dt_;
  double k_;
};

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, NULL, NULL);
  Log::open();

  int nsd = 2;
  int n_elem = 50;

  PetscOptionsGetInt(NULL, NULL, "-nsd", &nsd, NULL);
  PetscOptionsGetInt(NULL, NULL, "-n_elems", &n_elem, NULL);

  double t = 0.000;
  double dt = 0.001;
  double end_t = 0.010;

  int n_ts;
  PetscBool set_n_ts = PETSC_FALSE;
  PetscOptionsGetInt(NULL, NULL, "-n_ts", &n_ts, &set_n_ts);
  if (set_n_ts == PETSC_TRUE)
    end_t = t + dt * n_ts;

  {
    auto msh = BoxMeshGen(nsd, n_elem, n_elem, n_elem).generate();
    auto analytic_sol = std::make_shared<HTAnalyticSol>(nsd);
    auto ht_eq = std::make_shared<HTEquation>(analytic_sol, dt);
    auto solver = std::make_shared<PetscKSPSolver>(true /* IG nonzero */, true /* reuse matrix */);
    System<HTNodeData> system;

    system.set_equation(ht_eq);
    system.set_mesh(msh);
    system.set_solver(solver);
    system.allocate();

    // set boundary conditions
    system.set_bc([analytic_sol] (const auto& node, BoundaryCondition& bc) {
      if (node.is_on_surface()) {
        double val = analytic_sol->calc_at(node.position(), 0.0);
        bc.specify_value("u", val);
      }
    });

    // initial conditions
    for (auto nodeIt = system.lcl_nodes_begin(); !nodeIt.end(); nodeIt++) {
      nodeIt->data().u = analytic_sol->calc_at(nodeIt->position(), 0.0);
    }

    // write IC
    system.write("data");

    // timestepping
    while (t < end_t) {
      t += dt;
      system.solve();

      // print max error after this timestep
      double max_err = 0.0;
      for (auto it = system.lcl_nodes_begin(); !it.end(); it++) {
        it->data().u_analytical = analytic_sol->calc_at(it->position(), t);
        max_err = std::max(max_err, fabs(it->data().u - it->data().u_analytical));
      }

      double all_max_err = -1;
      MPI_Reduce(&max_err, &all_max_err, 1, MPI_DOUBLE, MPI_MAX, 0, PETSC_COMM_WORLD);
      LOG(LogInfo) << "Max error: " << all_max_err;

      system.write_timestep("data", t);
    }
  }

  PetscFinalize();
  return 0;
}
