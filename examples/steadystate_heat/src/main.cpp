#include <afem.h>

#ifndef M_PI
#    define M_PI 3.14159265358979323846
#endif

#include <vector>
#include <petsc/ksp.h>
#include <common/log.h>

struct HTNodeData {
  PetscScalar u;
  PetscScalar u_analytical;

  HTNodeData() : u(0.0), u_analytical(0.0) {}

  static const std::vector<NodeVariable> variables;
};

const std::vector<NodeVariable> HTNodeData::variables = {
	NodeVariable("u", offsetof(HTNodeData, u)),
	NodeVariable("u_analytical", offsetof(HTNodeData, u_analytical)),
};

class HTAnalyticSol {
 public:
  HTAnalyticSol(int nsd) : nsd_(nsd) {}

  inline double calc_at(const Vector3& pos) {
    if (nsd_ == 1)
      return sin(M_PI * pos.x());
    else if (nsd_ == 2)
      return sin(M_PI * pos.x()) * sin(M_PI * pos.y());
    else if (nsd_ == 3)
      return sin(M_PI * pos.x()) * sin(M_PI * pos.y()) * sin(M_PI * pos.z());

    throw NotImplementedException();
  }

  inline double calc_d2u(const Vector3& pt) {
    if (nsd_ == 1)
      return (-1 * M_PI * M_PI * sin(pt.x() * M_PI));
    else if (nsd_ == 2)
      return (-2 * M_PI * M_PI * sin(pt.x() * M_PI) * sin(pt.y() * M_PI));
    else if (nsd_ == 3)
      return (-3 * M_PI * M_PI * sin(pt.x() * M_PI) * sin(pt.y() * M_PI) * sin(pt.z() * M_PI));
  }

 private:
  int nsd_;
};

class HTEquation : public Equation<HTNodeData> {
  using Equation<HTNodeData>::MeshDataT;
 public:
  HTEquation(std::shared_ptr<HTAnalyticSol> analytic_sol)
    : analytic_sol_(analytic_sol) {}

  std::vector<std::string> solve_for() const {
    return { "u" };
  }

  void integrands_matrix(FEMElm& fe, ElemMatrix& ae, MeshDataT& data) override {
    ae -= fe.dN() * fe.dN().transpose() * fe.jacc_x_w();
  }

  void integrands_vector(FEMElm& fe, ElemVector& be, MeshDataT& data) override {
    Vector3 pt = fe.position();
    double force = analytic_sol_->calc_d2u(pt);
    be += fe.N() * force * fe.jacc_x_w();
  }

 private:
  std::shared_ptr<HTAnalyticSol> analytic_sol_;
};

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, NULL, NULL);
  Log::open();

  int nsd = 2;
  int n_elem = 50;

  PetscOptionsGetInt(NULL, NULL, "-nsd", &nsd, NULL);
  PetscOptionsGetInt(NULL, NULL, "-n_elems", &n_elem, NULL);

  char gmsh_path[512] = {};
  PetscOptionsGetString(NULL, NULL, "-from_gmsh", gmsh_path, sizeof(gmsh_path), NULL);

  {
    std::shared_ptr<Mesh> msh;
    if (gmsh_path[0] == '\0')
      msh = BoxMeshGen(nsd, n_elem, n_elem, n_elem).generate_distributed();
    else
      msh = load_gmsh_distributed(gmsh_path);

    auto analytic_sol = std::make_shared<HTAnalyticSol>(nsd);
    auto ht_eq = std::make_shared<HTEquation>(analytic_sol);
    auto solver = std::make_shared<PetscKSPSolver>(false /* zero initial guess */);
    System<HTNodeData> system;

    system.set_equation(ht_eq);
    system.set_mesh(msh);
    system.set_solver(solver);
    system.allocate();

    // set boundary conditions
    system.set_bc([analytic_sol, nsd](const auto& node, BoundaryCondition& bc) {
      static const double eps = 1e-6;
      bool on_surf = false;
      on_surf = on_surf || fabs(node.position().x()) < eps || fabs(node.position().x() - 1.0) < eps;
      if (nsd >= 2)
        on_surf = on_surf || fabs(node.position().y()) < eps || fabs(node.position().y() - 1.0) < eps;
      if (nsd >= 3)
        on_surf = on_surf || fabs(node.position().z()) < eps || fabs(node.position().z() - 1.0) < eps;

      if (on_surf) {
        double val = analytic_sol->calc_at(node.position());
        bc.specify_value("u", val);
      }
    });

    system.solve();

    double max_err = 0.0;
    for (auto it = system.lcl_nodes_begin(); !it.end(); it++) {
      double u_a = analytic_sol->calc_at(it->position());
      it->data().u_analytical = u_a;  // for visualization
      max_err = std::max(max_err, fabs(it->data().u - u_a));
    }

    double all_max_err = -1;
    MPI_Reduce(&max_err, &all_max_err, 1, MPI_DOUBLE, MPI_MAX, 0, PETSC_COMM_WORLD);
    LOG(LogInfo) << "Max error: " << all_max_err;

    system.write("data_final");
  }

  PetscFinalize();
  return 0;
}
