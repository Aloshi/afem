#pragma once

//#include <femelm.h>
#include <Eigen/Dense>

class FEMElm;

typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> ElemMatrix;  // elemental stiffness matrix type
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> ElemVector;  // elemental rhs vector contribution type

struct NoElemData {};

template <typename NodeData, typename ElemData = NoElemData>
class Equation
{
public:
  typedef MeshData<NodeData, ElemData> MeshDataT;

  virtual std::vector<std::string> solve_for() const = 0;
  virtual void integrands_matrix(FEMElm& fe, ElemMatrix& ae, MeshDataT& data) = 0;
  virtual void integrands_vector(FEMElm& fe, ElemVector& be, MeshDataT& data) = 0;
};
