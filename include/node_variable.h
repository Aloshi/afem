#pragma once

struct NodeVariable {
  const char* name;
  size_t offset;

  NodeVariable(const char* n, size_t off)
	  : name(n), offset(off) {}
};
