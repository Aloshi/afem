#pragma once

#include <memory>

#include <mesh.h>
#include <mesh_data.h>
#include <node_variable.h>

class MeshWriter
{
 public:
  template <typename NodeData, typename ElemData>
  void write(Mesh* mesh, MeshData<NodeData, ElemData>* data, const std::string& path) {
    write(mesh, (void*) data->node_data().data(), sizeof(NodeData), NodeData::variables, path);
  }

  template <typename NodeData, typename ElemData>
  void write_timestep(double t, Mesh* mesh, MeshData<NodeData, ElemData>* data, const std::string& path) {
    write_timestep(t, mesh, (void*) data->node_data().data(), sizeof(NodeData), NodeData::variables, path);
  }

  virtual void write(Mesh* mesh, void* node_data, size_t node_data_stride, const std::vector<NodeVariable>& variables, const std::string& path) = 0;
  virtual void write_timestep(double t, Mesh* mesh, void* node_data, size_t node_data_stride, const std::vector<NodeVariable>& variables, const std::string& path) = 0;
};

std::shared_ptr<Mesh> load_gmsh_distributed(const std::string& path);
