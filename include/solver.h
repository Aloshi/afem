#pragma once

#include <stdint.h>  // for uint64_t
#include <functional>
#include <petsc.h>

class Solver
{
 public:
  virtual ~Solver() {}

  typedef std::function<void(Vec prev_sol, Mat mat_out, Vec vec_out)> assembly_func_t;

  inline void set_assembly_callback(const assembly_func_t& f) {
    assembly_func_ = f;
  }

  virtual void init(Mat matrix) {}

  // sol should be initialized with the initial guess, if used
  virtual void solve(Vec sol) = 0;

 protected:
  assembly_func_t assembly_func_;

  inline void build_matrix(Vec prev_sol, Mat mat_out) {
    assembly_func_(prev_sol, mat_out, NULL);
  }
  inline void build_vector(Vec prev_sol, Vec rhs_out) {
    assembly_func_(prev_sol, NULL, rhs_out);
  }
  inline void build_mat_and_vec(Vec prev_sol, Mat mat_out, Vec rhs_out) {
    assembly_func_(prev_sol, mat_out, rhs_out);
  }
};
