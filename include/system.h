#pragma once

#include <equation.h>
#include <mesh.h>
#include <mesh_data.h>
#include <mesh_io/plt_writer.h>
#include <boundary_condition.h>
#include <node.h>
#include <element.h>
#include <femelm.h>
#include <solver.h>
#include <petsc/matrix.h>
#include <petsc/vector.h>
#include <common/exceptions.h>
#include <common/log.h>

#include <memory>  // for std::shared_ptr

template <typename NodeData, typename ElemData = NoElemData>
class System
{
 public:
  typedef std::function<void(const Node<NodeData, ElemData>&, BoundaryCondition&)> bc_func_t;
  typedef Equation<NodeData, ElemData> EquationT;

  System() : mesh_data_vec_(NULL), local_indices_is_(NULL), global_indices_is_(NULL) {}

  virtual ~System() {
    VecScatterDestroy(&mesh_data_to_sol_);
    ISDestroy(&local_indices_is_);
    ISDestroy(&global_indices_is_);
    VecDestroy(&mesh_data_vec_);
  }

  void set_equation(const std::shared_ptr<EquationT>& eq) {
    eq_ = eq;
    update_dof();
  }

  void set_mesh(const std::shared_ptr<Mesh>& new_msh) {
    assert(mesh_ == nullptr);
    mesh_ = new_msh;
    update_bc();
  }

  void set_solver(const std::shared_ptr<Solver>& new_solver) {
    solver_ = new_solver;
  }

  // allocate global matrix, vector, and mesh data
  void allocate() {
    assert(n_dof() > 0);
    assert(solver_ != nullptr);
    assert(mesh_data_ == nullptr);
    assert(mesh_data_vec_ == nullptr);
    assert(local_indices_is_ == nullptr);
    assert(global_indices_is_ == nullptr);

    const PetscInt n_rows = n_dof() * mesh_->n_glbl_nodes();
    const PetscInt n_lcl_rows = n_dof() * mesh_->n_lcl_nodes();
    const int scalars_per_nodedata = sizeof(NodeData) / sizeof(PetscScalar);

    // matrix
    mat_.create(n_rows, n_rows);

    // approximation: 3^nsd * (3 * basis - 2)
    PetscInt around_neighbor = 27 * n_dof();
    PetscInt dnz = around_neighbor;
    PetscInt onz = around_neighbor;
    mat_.preallocate(dnz, NULL, onz, NULL);
    MatSetOption(mat_, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);

    // vector
    solution_.create(n_rows);
    
    mesh_data_ = std::make_shared< MeshData<NodeData, ElemData> >(mesh_->n_lcl_nodes(), mesh_->n_lcl_elements());

    // vector backed by mesh data so we can use it in PETSc scatters
    VecCreateSeqWithArray(PETSC_COMM_SELF, n_dof(), mesh_->n_lcl_nodes() * scalars_per_nodedata,
                          (PetscScalar*) mesh_data_->node_data().data(), &mesh_data_vec_);

    // set up index set containing offsets into mesh_data_ with proper stride/offsets for mesh_data_vec_
    PetscInt* local_indices = NULL;
    PetscMalloc(mesh_->n_lcl_nodes() * sizeof(PetscInt) * n_dof(), (void**) &local_indices);

    for (LocalNodeID i = 0; i < mesh_->n_lcl_nodes(); i++) {
      for (int dof = 0; dof < n_dof(); dof++) {
        int idx = i * scalars_per_nodedata + (dof_vars_.at(dof)->offset / sizeof(PetscScalar));
        local_indices[i * n_dof() + dof] = idx;
      }
    }
    ISCreateGeneral(PETSC_COMM_SELF, n_lcl_rows, local_indices, PETSC_OWN_POINTER, &local_indices_is_);

    // set up index set containing global indices in the order that matches our local indices
    PetscInt* global_indices = NULL;
    PetscMalloc(mesh_->n_lcl_nodes() * sizeof(PetscInt), (void**) &global_indices);
    for (LocalNodeID i = 0; i < mesh_->n_lcl_nodes(); i++) {
      global_indices[i] = mesh_->local_to_global(i) * n_dof();
    }
    // index set takes ownership of global_indices (so no need to free)
    ISCreateBlock(PETSC_COMM_SELF, n_dof(), mesh_->n_lcl_nodes(), global_indices, PETSC_OWN_POINTER, &global_indices_is_);

    // create scatter (scatter all local values -> the global_indices array made above)
    VecScatterCreate(mesh_data_vec_, local_indices_is_, solution_, global_indices_is_, &mesh_data_to_sol_);

    // set up solver
    using namespace std::placeholders;
    solver_->set_assembly_callback(std::bind(&System::assemble, this, _1, _2, _3));
    solver_->init(mat_);
  }

  void set_bc(const bc_func_t& f) {
    bc_func_ = f;
    update_bc();
  }

  // iterators
  inline NodeIterator<NodeData, ElemData> lcl_nodes_begin() {
    assert(mesh_ != nullptr);
    return NodeIterator<NodeData, ElemData>(mesh_.get(), mesh_data_.get(), 0, mesh_->n_lcl_nodes());
  }
  inline ElemIterator<NodeData, ElemData> lcl_elems_begin() {
    assert(mesh_ != nullptr);
    return ElemIterator<NodeData, ElemData>(mesh_.get(), mesh_data_.get(), 0, mesh_->n_lcl_elements());
  }
  inline ElemIterator<NodeData, ElemData> owned_elems_begin() {
    assert(mesh_ != nullptr);

    if (mesh_->is_distributed())
      return ElemIterator<NodeData, ElemData>(mesh_.get(), mesh_data_.get(), 0, mesh_->n_lcl_elements());

    // if mesh is not distributed, everyone has the same mesh
    // we need to split up ownership of the elements somehow
    // simple way here: give each node n_nodes / mpi_size,
    // and give the last process the remainder (n_nodes % mpi_size)
    // not the *best* approximation (could distribute remainder more uniformly),
    // but it works
    int rank, size;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
    MPI_Comm_size(PETSC_COMM_WORLD, &size);

    unsigned int lcl_elms = mesh_->n_lcl_elements();
    LocalNodeID start = (lcl_elms / size) * rank + std::min((int) (lcl_elms % size), rank);
    LocalNodeID end = start + (lcl_elms / size) + (lcl_elms % size > rank ? 1 : 0);

    return ElemIterator<NodeData, ElemData>(mesh_.get(), mesh_data_.get(), start, end);
  }

  void solve(bool set_ic = true) {
    // set IC from node data
    if (set_ic) {
      copy_data_to_sol(solution_);
    }

    solver_->solve(solution_);

    // copy data from petsc solution vector into node data
    PetscScalar norm;
    VecNorm(solution_, NORM_INFINITY, &norm);
    LOG(LogInfo) << "solution norm: " << norm;

    copy_sol_to_data(solution_);
  }

  inline unsigned int n_dof() const {
    return dof_vars_.size();
  }

  void write(const std::string& name) {
    PetscBool no_io = PETSC_FALSE;
    PetscOptionsGetBool(NULL, NULL, "-no_io", &no_io, NULL);
    if (no_io)
      return;

    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    std::stringstream ss;
    ss << name << "_" << rank << ".plt";

    writer_.write(mesh_.get(), mesh_data_.get(), ss.str());
  }

  void write_timestep(const std::string& name, double t) {
    PetscBool no_io = PETSC_FALSE;
    PetscOptionsGetBool(NULL, NULL, "-no_io", &no_io, NULL);
    if (no_io)
      return;

    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    std::stringstream ss;
    ss << name << "_" << rank << ".plt";

    writer_.write_timestep(t, mesh_.get(), mesh_data_.get(), ss.str());
  }

 protected:
  void assemble(Vec prev_sol, Mat mat, Vec vec) {
    if (prev_sol) {
      copy_sol_to_data(prev_sol);
    }

    bool assemble_matrix = (mat != NULL);
    bool assemble_vector = (vec != NULL);

    assert(mesh_ != NULL);
    assert(mesh_data_ != NULL);
    assert(eq_);

    std::vector<PetscInt> rows, cols;
    for (auto it = owned_elems_begin(); !it.end(); it++) {
      FEMElm fe;
      fe.refill(*it);

      ElemMatrix ae = ElemMatrix::Zero(fe.nbf() * n_dof(), fe.nbf() * n_dof());
      ElemVector be = ElemVector::Zero(fe.nbf() * n_dof());

      while (fe.next_itg_pt()) {
        if (assemble_matrix)
          eq_->integrands_matrix(fe, ae, *mesh_data_.get());
        if (assemble_vector)
          eq_->integrands_vector(fe, be, *mesh_data_.get());
      }

      calc_ae_be_indices(*it, &rows, &cols);
      if (assemble_matrix) {
        MatSetValues(mat, rows.size(), rows.data(), cols.size(), cols.data(), ae.data(), ADD_VALUES);
      }
      if (assemble_vector) {
        VecSetValues(vec, rows.size(), rows.data(), be.data(), ADD_VALUES);
      }
    }

    if (mat) {
      MatAssemblyBegin(mat, MAT_FINAL_ASSEMBLY);
      MatAssemblyEnd(mat, MAT_FINAL_ASSEMBLY);
    }
    if (vec) {
      VecAssemblyBegin(vec);
      VecAssemblyEnd(vec);
    }

    apply_essential_bc(mat, vec);
  }

  void calc_ae_be_indices(const Element<NodeData, ElemData>& elem,
                          std::vector<PetscInt>* rows_out,
                          std::vector<PetscInt>* cols_out) {
    rows_out->resize(elem.n_nodes() * n_dof());
    cols_out->resize(elem.n_nodes() * n_dof());

    for (unsigned int i = 0; i < elem.n_nodes(); i++) {
      for (unsigned int j = 0; j < n_dof(); j++) {
        PetscInt mat_row = get_row(elem.node(i), j);
        unsigned int idx = i * n_dof() + j;

        rows_out->at(idx) = mat_row;
        cols_out->at(idx) = mat_row;
      }
    }
  }

  void apply_essential_bc(Mat mat, Vec vec) {
    PetscInt n_rows = ess_bc_rows_.size();
    PetscInt* rows = ess_bc_rows_.data();
    PetscScalar* vals = ess_bc_values_.data();

    if (mat) {
      MatZeroRows(mat, n_rows, rows, 1.0, NULL, NULL);
    }
    if (vec) {
      VecSetValues(vec, n_rows, rows, vals, INSERT_VALUES);
      VecAssemblyBegin(vec);
      VecAssemblyEnd(vec);
    }
  }

  void update_dof() {
    dof_vars_.clear();
    if (eq_) {
      const auto solve_var_names = eq_->solve_for();
      for (auto name = solve_var_names.begin(); name != solve_var_names.end(); name++) {
        auto search = std::find_if(NodeData::variables.begin(), NodeData::variables.end(),
          [&](const NodeVariable& v) -> bool {
            return (v.name == *name);
          }
        );
        if (search == NodeData::variables.end()) {
          throw AFEMException() << "Unknown solve variable '" << *name << "'";
        }

        // node variables must be aligned on offsets of 8 for index set to work
        assert(search->offset % sizeof(PetscScalar) == 0);
        dof_vars_.push_back(&*search);
      }
    }
  }

  unsigned int variable_name_to_dof(const std::string& name) {
    for (unsigned int i = 0; i < dof_vars_.size(); i++) {
      if (dof_vars_.at(i)->name == name)
        return i;
    }
    throw AFEMException() << "Variable '" << name << "' not found";
  }

  void update_bc() {
    ess_bc_rows_.clear();
    ess_bc_values_.clear();

    // do we have any BC specified?
    if (bc_func_ == nullptr)
      return;

    for (auto it = lcl_nodes_begin(); !it.end(); it++) {
      BoundaryCondition bc(it->local_id());
      bc_func_(*it, bc);

      if (!bc.empty()) {
        const auto& essBc = bc.essential_bc();
        for (auto essIt = essBc.begin(); essIt != essBc.end(); essIt++) {
          unsigned int dof = variable_name_to_dof(essIt->first);
          ess_bc_rows_.push_back(get_row(*it, dof));
          ess_bc_values_.push_back(essIt->second);
        }
      }
    }
  }

  inline PetscInt get_row(LocalNodeID id, unsigned int dof) const {
    return mesh_->local_to_global(id) * n_dof() + dof;
  }
  inline PetscInt get_row(const Node<NodeData, ElemData>& node, unsigned int dof) const {
    return get_row(node.local_id(), dof);
  }

  // scatters mesh_data_vec_ (i.e. mesh_data_) to sol
  void copy_data_to_sol(Vec sol) {
    VecScatterBegin(mesh_data_to_sol_, mesh_data_vec_, sol, INSERT_VALUES, SCATTER_FORWARD);
    VecScatterEnd(mesh_data_to_sol_, mesh_data_vec_, sol, INSERT_VALUES, SCATTER_FORWARD);
    // VecAssemblyBegin(sol);
    // VecAssemblyEnd(sol);
  }

  // scatters sol to mesh_data_vec_ (i.e. mesh_data_)
  void copy_sol_to_data(Vec sol) {
    VecScatterBegin(mesh_data_to_sol_, sol, mesh_data_vec_, INSERT_VALUES, SCATTER_REVERSE);
    VecScatterEnd(mesh_data_to_sol_, sol, mesh_data_vec_, INSERT_VALUES, SCATTER_REVERSE);
  }

 private:
  std::vector<const NodeVariable*> dof_vars_;

  std::shared_ptr< MeshData<NodeData, ElemData> > mesh_data_;  // managed by System
  std::shared_ptr<Mesh> mesh_;  // from outside
  std::shared_ptr<EquationT> eq_;  // from outside

  bc_func_t bc_func_;

  std::vector<PetscInt> ess_bc_rows_;
  std::vector<PetscScalar> ess_bc_values_;

  std::shared_ptr<Solver> solver_;
  PetscMatrix mat_;
  PetscVector solution_;

  // PETSc vector with storage backed by mesh_data_
  Vec mesh_data_vec_;

  // PETSc index set mapping solution_ -> mesh_data_vec_
  IS local_indices_is_;
  IS global_indices_is_;
  VecScatter mesh_data_to_sol_;

  unsigned int owned_elms_start_;
  unsigned int owned_elms_end_;

  PLTWriter writer_;
};
