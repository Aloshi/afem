#pragma once

#include <mesh.h>

#include <memory>

// Interface for generating a mesh
class IMeshGen
{
 public:
  virtual std::shared_ptr<Mesh> generate() = 0;
  virtual std::shared_ptr<Mesh> generate_distributed() {
    throw NotImplementedException() << "Distributed mesh gen not implemented";
  }
};
