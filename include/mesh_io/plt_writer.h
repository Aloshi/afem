#pragma once

#include <mesh_io.h>
#include <mesh_io/TecplotIO_ascii.h>

class PLTWriter : public MeshWriter {
 public:
	 using MeshWriter::write;
	 using MeshWriter::write_timestep;

  void write(Mesh* mesh, void* node_data, size_t node_data_stride,
    const std::vector<NodeVariable>& variables, const std::string& file) override;

  void write_timestep(double t, Mesh* mesh, void* node_data, size_t node_data_stride,
    const std::vector<NodeVariable>& variables, const std::string& file) override;

 private:
  TecplotWriterASCII w_;

  void write_data(TecplotZone& zone, Mesh* mesh, void* node_data,
    size_t node_data_stride, const std::vector<NodeVariable>& variables);
};
