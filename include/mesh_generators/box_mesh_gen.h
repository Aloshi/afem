#pragma once

#include <mesh_gen.h>

class BoxMeshGen : public IMeshGen
{
 public:
  BoxMeshGen(unsigned int nsd, unsigned int n_elms_x, unsigned int n_elms_y = 0, unsigned int n_elms_z = 0);

  std::shared_ptr<Mesh> generate() override;
  std::shared_ptr<Mesh> generate_distributed() override;

 private:
  unsigned int node_index(unsigned int x, unsigned int y, unsigned int z) const {
    return z * ((n_elms_[0]+1) * (n_elms_[1]+1)) + y * (n_elms_[0] + 1) + x;
  }

  inline unsigned int node_index(unsigned int x, unsigned int y) const {
    return node_index(x, y, 0);
  }
  inline unsigned int node_index(unsigned int x) const {
    return node_index(x, 0, 0);
  }

  unsigned int nsd_;
  unsigned int n_elms_[3];
};
