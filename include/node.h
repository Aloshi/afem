#pragma once

#include <mesh.h>
#include <mesh_data.h>

template <typename NodeData, typename ElemData>
class Node
{
 public:
  Node(Mesh* msh, MeshData<NodeData, ElemData>* data, LocalNodeID id)
    : id_(id), msh_(msh), msh_data_(data) {}

  inline LocalNodeID local_id() const { return id_; }
  inline GlobalNodeID global_id() const { return msh_->local_to_global(id_); }
  inline const Vector3& position() const { return msh_->node_pos(id_); }

  inline NodeData& data() { return msh_data_->node_data(id_); }
  inline const NodeData& data() const { return msh_data_->node_data(id_); }

  inline bool is_on_surface() const {
	  return msh_->node_indicators(id_) != 0;
  }

  inline Mesh* mesh() const {
    return msh_;
  }

 private:
  LocalNodeID id_;
  Mesh* msh_;
  MeshData<NodeData, ElemData>* msh_data_;
};

template <typename NodeData, typename ElemData>
class NodeIterator
{
 public:
	 NodeIterator(Mesh* msh, MeshData<NodeData, ElemData>* msh_data,
      LocalNodeID start_node, LocalNodeID end_node)
		 : msh_(msh), msh_data_(msh_data), cur_id_(start_node),
		 end_id_(end_node), node_(msh, msh_data, start_node) {}

  inline bool operator==(const NodeIterator<NodeData, ElemData>& rhs) {
    return cur_id_ == rhs.cur_id_;
  }
  inline bool operator!=(const NodeIterator<NodeData, ElemData>& rhs) {
    return !(*this == rhs);
  }

  inline const NodeIterator<NodeData, ElemData>& operator++(int) {
    node_ = Node<NodeData, ElemData>(msh_, msh_data_, ++cur_id_);
    return *this;
  }

  // deref operators
  inline Node<NodeData, ElemData>& operator*() {
    return node_;
  }
  inline Node<NodeData, ElemData>* operator->() {
    return &node_;
  }

  inline bool end() const {
    return cur_id_ >= end_id_;
  }

 private:
  LocalNodeID cur_id_;
  LocalNodeID end_id_;

  Node<NodeData, ElemData> node_;
  Mesh* msh_;
  MeshData<NodeData, ElemData>* msh_data_;
};
