#pragma once

#include <mesh.h>

template<typename NodeData, typename ElemData>
class MeshData
{
 public:
  MeshData(LocalNodeID n_nodes, ElemID n_elements) {
    node_data_.resize(n_nodes);
    elem_data_.resize(n_elements);
  }

  inline std::vector<NodeData>& node_data() {
    return node_data_;
  }
  inline std::vector<ElemData>& elem_data() {
    return elem_data_;
  }

  inline NodeData& node_data(LocalNodeID node_id) {
    return node_data_.at(node_id);
  }
  inline const NodeData& node_data(LocalNodeID node_id) const {
    return node_data_.at(node_id);
  }

  inline ElemData& elem_data(ElemID elem_id) {
    return elem_data_.at(elem_id);
  }
  inline const ElemData& elem_data(ElemID elem_id) const {
    return elem_data_.at(elem_id);
  }

 private:
  std::vector<NodeData> node_data_;
  std::vector<ElemData> elem_data_;
};
