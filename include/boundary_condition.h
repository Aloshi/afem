#pragma once

#include <mesh.h>

#include <map>

class BoundaryCondition
{
 public:
  BoundaryCondition(LocalNodeID node_id) : node_id_(node_id) {}

  // essential values, i.e. dirichlet
  inline void specify_value(const std::string& var_name, double value) {
    essential_[var_name] = value;
  }

  inline bool empty() const {
    return essential_.empty();
  }

  inline const std::map<std::string, double>& essential_bc() {
    return essential_;
  }

 private:
  LocalNodeID node_id_;
  std::map<std::string, double> essential_;
};
