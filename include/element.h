#pragma once

#include <mesh.h>
#include <mesh_data.h>
#include <node.h>

template <typename NodeData, typename ElemData>
class Element
{
 public:
  Element() : id_(0), msh_(NULL), msh_data_(NULL) {}

  Element(Mesh* msh, MeshData<NodeData, ElemData>* msh_data, ElemID id)
	  : id_(id), msh_(msh), msh_data_(msh_data) {}

  inline ElemID local_id() const {
    return id_;
  }

  inline unsigned int n_nodes() const {
    return msh_->elem_connectivity(id_).size();
  }
  inline Node<NodeData, ElemData> node(unsigned int lcl_node_idx) {
    return Node<NodeData, ElemData>(msh_, msh_data_, msh_->elem_connectivity(id_).at(lcl_node_idx));
  }
  inline const Node<NodeData, ElemData> node(unsigned int lcl_node_idx) const {
    return Node<NodeData, ElemData>(msh_, msh_data_, msh_->elem_connectivity(id_).at(lcl_node_idx));
  }
  inline ElemType type() const {
    return msh_->elem_type(id_);
  }

  inline ElemData& data() {
    return msh_data_->elem_data_.at(id_);
  }
  inline const ElemData& data() const {
    return msh_data_->elem_data_.at(id_);
  }

  inline Mesh* mesh() const {
    return msh_;
  }

 private:
  ElemID id_;
  Mesh* msh_;
  MeshData<NodeData, ElemData>* msh_data_;
};

template <typename NodeData, typename ElemData>
class ElemIterator
{
 public:
	 ElemIterator(Mesh* msh, MeshData<NodeData, ElemData>* msh_data, ElemID start_elem, ElemID end_elem)
		 : msh_(msh), msh_data_(msh_data), cur_id_(start_elem), end_id_(end_elem),
       elem_(msh, msh_data, start_elem) {}

  inline bool operator==(const ElemIterator<NodeData, ElemData>& rhs) {
    return cur_id_ == rhs.cur_id_;
  }
  inline bool operator!=(const ElemIterator<NodeData, ElemData>& rhs) {
    return !(*this == rhs);
  }

  inline ElemIterator<NodeData, ElemData>& operator++(int) {
    elem_ = Element<NodeData, ElemData>(msh_, msh_data_, ++cur_id_);
    return *this;
  }

  // deref operators
  inline Element<NodeData, ElemData>& operator*() {
    return elem_;
  }
  inline Element<NodeData, ElemData>* operator->() {
    return &elem_;
  }

  inline bool end() const {
    return cur_id_ >= end_id_;
  }

 private:
  ElemID cur_id_;
  ElemID end_id_;

  Element<NodeData, ElemData> elem_;
  Mesh* msh_;
  MeshData<NodeData, ElemData>* msh_data_;
};
