#pragma once

#include <petsc.h>

class PetscMatrix
{
 public:
  PetscMatrix() : comm_(PETSC_COMM_WORLD), mat_(NULL) {}

  virtual ~PetscMatrix() {
    if (mat_)
      MatDestroy(&mat_);
  }

  virtual void create(PetscInt n_rows, PetscInt n_cols) {
    MatCreate(comm_, &mat_);
    MatSetType(mat_, MATMPIAIJ);
    MatSetSizes(mat_, PETSC_DECIDE, PETSC_DECIDE, n_rows, n_cols);
    //MatSetFromOptions(mat_);
  }

  inline void preallocate(PetscInt n_diag_nz, PetscInt* diag_nz, PetscInt n_offdiag_nz, PetscInt* offdiag_nz) {
    MatMPIAIJSetPreallocation(mat_, n_diag_nz, diag_nz, n_offdiag_nz, offdiag_nz);
  }

  inline operator Mat() const {
    return mat_;
  }

 private:
  MPI_Comm comm_;
  Mat mat_;
};

