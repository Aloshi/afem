#pragma once

#include <solver.h>
#include <petsc.h>

// for linear problems
class PetscKSPSolver : public Solver
{
 public:
  PetscKSPSolver(bool initial_guess_nonzero = true, bool reuse_matrix = false)
    : comm_(PETSC_COMM_WORLD), ksp_(NULL),
      initial_guess_nonzero_(initial_guess_nonzero),
      reuse_matrix_(reuse_matrix), calculated_matrix_(false) {}

  void init(Mat mat) override {
    KSPCreate(comm_, &ksp_);
    KSPSetOperators(ksp_, mat, mat);
    KSPSetFromOptions(ksp_);
    KSPSetUp(ksp_);

    KSPSetInitialGuessNonzero(ksp_,
        initial_guess_nonzero_ ? PETSC_TRUE : PETSC_FALSE);
  }

  virtual ~PetscKSPSolver() {
    if (ksp_)
      KSPDestroy(&ksp_);
  }

  // sol should be filled with the initial guess if ig_nonzero is set
  void solve(Vec sol) override {
    Mat mat;
    KSPGetOperators(ksp_, &mat, NULL);

    Vec rhs;
    VecDuplicate(sol, &rhs);

    if (!reuse_matrix_ || !calculated_matrix_)
      MatZeroEntries(mat);

    // always recalculate vector
    VecZeroEntries(rhs);

    try {
      LOG(LogInfo) << "Assembling matrix/vector system...";
      double t0 = MPI_Wtime();
      if (!reuse_matrix_ || !calculated_matrix_) {
        build_mat_and_vec(NULL, mat, rhs);
        calculated_matrix_ = true;
      } else {
        build_vector(NULL, rhs);
      }
      double t1 = MPI_Wtime();
      LOG(LogInfo) << "Assembled mat/vec in " << (t1 - t0) << "s.";
    } catch(...) {
      VecDestroy(&rhs);
      throw;
    }

    LOG(LogInfo) << "Starting KSP solve...";
    double t0 = MPI_Wtime();
    KSPSolve(ksp_, rhs, sol);
    double t1 = MPI_Wtime();
    LOG(LogInfo) << "KSP solve completed in " << (t1 - t0) << "s.";
    VecDestroy(&rhs);
  }

 private:
  MPI_Comm comm_;
  KSP ksp_;
  bool initial_guess_nonzero_;
  bool reuse_matrix_;
  bool calculated_matrix_;
};
