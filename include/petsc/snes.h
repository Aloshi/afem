#pragma once

#include <petsc/sys.h>

// for non-linear problems
class PetscSNESSolver : public Solver
{
 public:
  PetscSNESSolver()
    : comm_(PETSC_COMM_WORLD), snes_(NULL), ksp_(NULL) {}

  void init(Mat mat) override {
    // create vector to store residual
    VecCreate(comm_, &residual_);
    // TODO resize it to match number of rows in mat

    SNESCreate(comm_, &snes_);
    SNESSetJacobian(snes_, mat, mat, &PetscSNESSolver::build_mat, (void*) this);
    SNESSetFunction(snes_, residual_, &PetscSNESSolver::calc_residual, (void*) this);
    SNESSetFromOptions(snes_);
  }

  virtual ~PetscSNESSolver() {
    if (snes_)
      SNESDestroy(&snes_);
    if (residual_)
      VecDestroy(&residual_);
  }

  // sol should be initialized with the initial guess,
  // will become solution after solve completes
  void solve(Vec sol) override {
    SNESSolve(snes_, NULL, sol);
  }

 private:
  static PetscErrorCode build_mat(SNES snes, Vec sol, Mat mat_out, Mat pmat_out, void* ctx) {
    PetscSNESSolver* solver = (PetscSNESSolver*) ctx;

    // re-assemble matrix for based on sol
    solver->build_matrix(sol, mat_out);

    return 0;
  }

  // x = solution to evaluate residual of
  static PetscErrorCode calc_residual(SNES snes, Vec x, Vec residual_out, void* ctx) {
    PetscSNESSolver* solver = (PetscSNESSolver*) ctx;

    // re-assemble rhs from x
    solver->build_vector(x, residual_out);

    return 0;
  }

  MPI_Comm comm_;
  SNES snes_;
  Vec residual_;
};
