#pragma once

#include <petsc.h>

class PetscVector
{
 public:
  PetscVector() : comm_(PETSC_COMM_WORLD), vec_(NULL) {}

  virtual ~PetscVector() {
    if (vec_)
      VecDestroy(&vec_);
  }

  void create(PetscInt n_rows) {
    VecCreate(comm_, &vec_);
    VecSetSizes(vec_, PETSC_DECIDE, n_rows);
    VecSetType(vec_, VECMPI);
    //VecSetFromOptions(vec_);
  }

  inline operator Vec() {
    return vec_;
  }

 private:
  MPI_Comm comm_;
  Vec vec_;
};
