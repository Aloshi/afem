#pragma once

#include <string>
#include <Eigen/Dense>
#include <petsc.h>  // for PetscScalar

/**
 * Flags specifying what values to calculate in calc_values().
 */
enum BasisFlags : unsigned int {
  // what do we need calculated for our equation?
  BASIS_POSITION = (1 << 0),
  BASIS_FIRST_DERIVATIVE = (1 << 1),

  BASIS_ALL = ((unsigned int)(~0x0))
};

/**
 * Per-integration-point values.
 */
struct BasisValues {
  static const int MAX_NSD = 3;  // max nsd (axes to take derivatives in)
  static const int MAX_N = 2*2*2;  // max number of shape functions, linear hex upper limit

  // only calculated if BASIS_POSITION is set
  Eigen::Vector3d position;  ///< global position of integration point

  // Constants that might not be constant when calculating non-standard points
  Eigen::Vector3d itg_pt; ///< isoparametric position of integration point, always

  // shape function, always
  Eigen::Matrix<PetscScalar, Eigen::Dynamic, 1, 0, MAX_N> N;

  // first derivative of N in isoparametric space, BASIS_FIRST_DERIVATIVE
  Eigen::Matrix<PetscScalar, Eigen::Dynamic, Eigen::Dynamic, 0, MAX_N, MAX_NSD> dNde; 

  // first derivative (only calculated if BASIS_FIRST_DERIVATIVE is set)
  Eigen::Matrix<PetscScalar, Eigen::Dynamic, Eigen::Dynamic, 0, MAX_N, MAX_N> dN;  ///< dN/dx[shape_func][axis]
  Eigen::Matrix<PetscScalar, Eigen::Dynamic, Eigen::Dynamic, 0, MAX_NSD, MAX_NSD> dXde;  ///< jacobian matrix
  Eigen::Matrix<PetscScalar, Eigen::Dynamic, Eigen::Dynamic, 0, MAX_NSD, MAX_NSD> cof;  ///< cofactor matrix of jacobian matrix

  double jacobian;  ///< determinant of dXde
  double jacc_x_weight;  ///< jacobian scaled by integration point weight
};

/**
 * Basis functions.
 */
enum kBasisFunction {
  BASIS_NONE = 0,  ///< no basis function
  BASIS_LINEAR = 1,  ///< linear basis function
  BASIS_QUADRATIC = 2,  ///< quadratic basis function
  BASIS_CUBIC = 3,  ///< cubic basis function
};

/**
 * Convert a lowercase string to a basis function enum value.
 * (e.g. "linear" => BASIS_LINEAR)
 *
 * @param str string to convert
 * @returns string as enum value
 * @throw TALYException if string is unknown
 */
kBasisFunction basis_string_to_enum(const std::string& str);

/**
 * Convert a basis function enum to a string.
 * @param bf basis function enum value
 * @returns string version of bf (ex: BASIS_LINEAR => "linear")
 */
const char* basis_enum_to_string(kBasisFunction bf);

/**
 * Calculate the "mesh order" of a basis function.
 * Ex: BASIS_LINEAR => 1, BASIS_QUADRATIC => 2.
 * Matches legacy "orderOfBF" value.
 * @param bf basis function
 * @returns "mesh order" of bf
 */
int basis_get_mesh_order(kBasisFunction bf);
