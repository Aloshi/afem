#pragma once

#include <basis/basis.h>
#include <basis/elemnodes.h>

#include <Eigen/Dense>

class BasisCommon
{
 public:
  BasisCommon(int nsd, int nbf, int nbf_per_node, double jacobian_scale)
    : nsd_(nsd), nbf_(nbf), nbf_per_node_(nbf_per_node), jacobian_scale_(jacobian_scale) {}

  virtual void calc_values(const decltype(BasisValues::itg_pt)& itg_pt, double weight, const ElemNodes& elem,
                           unsigned int flags, BasisValues* vals_out);

  inline int nsd() const { return nsd_; }
  inline int nbf() const { return nbf_; }
  inline int nbf_per_node() const { return nbf_per_node_; }

 protected:
  /**
   * Calculate shape functions.
   * @param localPt integration point
   * @param[out] n_out shape functions evaluated at localPt (output)
   */
  virtual decltype(BasisValues::N) calc_N(const Eigen::Vector3d& itg_pt) = 0;

  /**
   * Calculate dNde values at integration point.
   * This could be simplified to use the nsd == 3 case all the time, but
   * that would rely on multiply-by-zero, which may not get optimized out
   * (due to NaN propagation).
   * @param localPt integration point
   * @param[out] dnde_out derivative of N in isoparametric space (output)
   */
  virtual decltype(BasisValues::dNde) calc_dNde(const Eigen::Vector3d& itg_pt) = 0;

  Eigen::Vector3d calc_position(const decltype(BasisValues::N)& n, const ElemNodes& elem);
  decltype(BasisValues::dXde) calc_dXde(const decltype(BasisValues::dNde)& dnde, const ElemNodes& elem);
  decltype(BasisValues::dN) calc_dN(const decltype(BasisValues::dNde)& dnde, const decltype(BasisValues::cof)& cof, double jacc);
  decltype(BasisValues::cof) calc_cofactor(const decltype(BasisValues::dXde)& dxde);

  const int nsd_;
  const int nbf_;
  const int nbf_per_node_;
  const double jacobian_scale_;
};
