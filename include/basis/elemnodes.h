/*
  Copyright 2014-2016 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#pragma once

#include <vector>
#include <Eigen/Dense>

/**
 * Accessor for accessing the nodes of a particular element.
 * Simplifies passing around an (ELEM*, GRID*) pair all the time.
 */
struct ElemNodes {
 public:
  ElemNodes() {}

  /**
   * @param nodes node locations
   * @param n_nodes number of node locations
   */
  inline ElemNodes(std::vector<Eigen::Vector3d>&& nodes)
    : nodes_(nodes) {}

  inline ElemNodes(const Eigen::Vector3d* nodes, int n_nodes) {
    nodes_.resize(n_nodes);
    for (int i = 0; i < n_nodes; i++) {
      nodes_[i] = nodes[i];
    }
  }

  /**
   * Get the location of node i.
   * @param i element-local node index
   * @returns node i
   */
  inline const Eigen::Vector3d& node_pos(int i) const {
    return nodes_.at(i);
  }

 private:
  std::vector<Eigen::Vector3d> nodes_;
};