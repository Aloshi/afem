#pragma once

#include <basis/basis_common.h>

class BoxLinearBasis : public BasisCommon {
 public:
  BoxLinearBasis(int nsd) : BasisCommon(nsd, (int)pow(2, nsd), 1, 1.0) {}

 protected:
  decltype(BasisValues::N) calc_N(const Eigen::Vector3d& localPt) override;
  decltype(BasisValues::dNde) calc_dNde(const Eigen::Vector3d& localPt) override;
};
