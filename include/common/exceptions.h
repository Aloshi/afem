#pragma once

#include <exception>
#include <string>
#include <sstream>

class AFEMException : public std::exception {
 public:
  virtual ~AFEMException() throw() {}

  virtual const char* what() const noexcept override { return msg_.c_str(); }

  template<typename T>
  friend AFEMException operator<<(AFEMException e, const T& obj);

 protected:
  std::string msg_;
};

template<typename T>
AFEMException operator<<(AFEMException e, const T& obj) {
  std::stringstream ss;
  ss << e.msg_ << obj;
  e.msg_ = ss.str();
  return e;
}

/**
 * Exception thrown when an option as not been implemented.
 */
class NotImplementedException : public AFEMException {};

class FileIOException : public AFEMException {};
