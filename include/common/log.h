#pragma once

#define LOG(level) Log().getStream(level)

#include <string>
#include <sstream>
#include <assert.h>
#include <iostream>

enum LogLevel : int { LogTrace = 0, LogDebug = 1, LogInfo = 2, LogWarning = 3, LogError = 4, LogFatal = 5 };

class Log
{
public:
	~Log();
	std::ostringstream& getStream(LogLevel level);

	static std::string getLogPath();

	static void flush();
	static void open();
	static void close();

protected:
	std::ostringstream mStream;
	static FILE* sFile;
  static int sRank;

private:
	LogLevel mMsgLevel;
};
