#pragma once

#include <vector>

#include <Eigen/Dense>
#include <petsc.h>  // for PetscInt

#include <common/exceptions.h>

typedef unsigned int LocalNodeID;
typedef PetscInt GlobalNodeID;
typedef unsigned int ElemID;
typedef Eigen::Vector3d Vector3;

typedef unsigned int SurfaceIndicators;
#define NODE_INDICATOR_MARKER "node_indicators"
#define NODE_INDICATOR_FORMAT "%d"

enum ElemType : int {
  ELEM_LINE,
  ELEM_QUAD,
  ELEM_HEX,
};

// Discretization of the domain
class Mesh
{
 public:
  Mesh(std::vector<Vector3>&& pos, std::vector<SurfaceIndicators>&& surfs,
    std::vector< std::vector<LocalNodeID> >&& conn, std::vector< ElemType>&& types)
      : node_positions_(pos), node_indicators_(surfs),
        elem_connectivity_(conn), elem_types_(types) {}

  // for distributed meshes
  Mesh(std::vector<Vector3>&& pos, std::vector<SurfaceIndicators>&& surfs,
    std::vector< std::vector<LocalNodeID> >&& conn, std::vector< ElemType>&& types,
    std::vector<GlobalNodeID>&& lcl_to_glb_node, std::vector<ElemID>&& lcl_to_glb_elem,
    GlobalNodeID n_glb_nodes, ElemID n_glb_elems)
      : node_positions_(pos), node_indicators_(surfs),
        elem_connectivity_(conn), elem_types_(types),
        is_distributed_(true),
        local_to_global_node_(lcl_to_glb_node),
        local_to_global_elem_(lcl_to_glb_elem),
        n_glb_nodes_(n_glb_nodes), n_glb_elems_(n_glb_elems) {}

  inline bool is_distributed() const { return is_distributed_; }
  inline LocalNodeID n_lcl_nodes() const { return node_positions_.size(); }
  inline ElemID n_lcl_elements() const { return elem_connectivity_.size(); }

  inline GlobalNodeID local_to_global(LocalNodeID id) {
    if (is_distributed())
      return local_to_global_node_.at(id);
    else
      return (GlobalNodeID) id;
  }

  inline LocalNodeID global_to_local(GlobalNodeID id) {
    if (is_distributed())
      throw NotImplementedException();
    else
      return (LocalNodeID) id;
  }

  inline GlobalNodeID n_glbl_nodes() const {
    if (is_distributed())
      return n_glb_nodes_;
    else
      return n_lcl_nodes();
  }

  inline ElemID n_glbl_elements() const {
    if (is_distributed())
      return n_glb_elems_;
    else
      return n_lcl_elements();
  }

  inline const Vector3& node_pos(LocalNodeID id) const {
    return node_positions_.at(id);
  }

  inline SurfaceIndicators node_indicators(LocalNodeID id) const {
    return node_indicators_.at(id);
  }

  inline const std::vector<LocalNodeID>& elem_connectivity(ElemID id) const {
    return elem_connectivity_.at(id);
  }

  inline const std::vector< std::vector<LocalNodeID> > elem_connectivity() const {
    return elem_connectivity_;
  }

  inline ElemType elem_type(ElemID id) const {
    return elem_types_.at(id);
  }

  inline const std::vector<ElemType>& elem_types() const {
    return elem_types_;
  }

 private:
  std::vector<Vector3> node_positions_;
  std::vector<SurfaceIndicators> node_indicators_;

  std::vector< std::vector<LocalNodeID> > elem_connectivity_;
  std::vector<ElemType> elem_types_;

  // for distributed meshes
  bool is_distributed_;
  std::vector<ElemID> local_to_global_elem_;
  std::vector<GlobalNodeID> local_to_global_node_;
  GlobalNodeID n_glb_nodes_;
  ElemID n_glb_elems_;
};
