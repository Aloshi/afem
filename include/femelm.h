#pragma once

#include <Eigen/Dense>

#include <element.h>
#include <basis/basis.h>
#include <basis/basis_common.h>
#include <mesh.h>
#include <common/log.h>

typedef std::pair<Eigen::Vector3d, double> GaussPoint;

class FEMElm {
 public:
  void refill(Mesh* mesh, ElemID elem_id);

  template <typename NodeData, typename ElemData>
  inline void refill(const Element<NodeData, ElemData>& elem) {
    refill(elem.mesh(), elem.local_id());
  }

  bool next_itg_pt();

  inline const Eigen::Vector3d& position() const { return values_.position; }
  inline const Eigen::Vector3d& itg_pt() const { return values_.itg_pt; }
  inline int cur_itg_pt_num() const { return cur_itg_pt_; }

  inline const decltype(BasisValues::N)& N() const { return values_.N; }
  inline double N(int i) const { return values_.N(i); }

  inline const decltype(BasisValues::dNde)& dNde() const { return values_.dNde; }
  inline double dNde(int i, int j) const { return values_.dNde(i, j); }

  inline const decltype(BasisValues::dXde)& dXde() const { return values_.dXde; }
  inline double dXde(int i, int j) const { return values_.dXde(i, j); }

  inline const decltype(BasisValues::cof)& cof() const { return values_.cof; }
  inline double cof(int i, int j) const { return values_.cof(i, j); }

  inline const decltype(BasisValues::dN)& dN() const { return values_.dN; }
  inline double dN(int i, int j) const { return values_.dN(i, j); }

  inline double unweighted_jacc() const { return values_.jacobian; }
  inline double jacc_x_w() const { return values_.jacc_x_weight; }

  inline int nbf() const { return bf_->nbf(); }
  inline int nbf_per_nodes() const { return bf_->nbf_per_node(); }
  inline int nsd() const { return bf_->nsd(); }

  template <typename NodeData, typename ElemData>
  inline double variable(MeshData<NodeData, ElemData>& data, size_t offset) const {
    double out = 0.0;
    const auto& conn = mesh_->elem_connectivity(elem_id_);
    for (int i = 0; i < nbf(); i++) {
      LocalNodeID node_id = conn.at(i);
      const NodeData* node_data = &data.node_data(node_id);
      double val = *((double*) (( (char*) node_data) + offset));
      out += N(i) * val;
    }
    return out;
  }

 private:
  BasisCommon* bf_;
  ElemNodes nodes_;

  Mesh* mesh_;
  ElemID elem_id_;

  std::vector<GaussPoint>* itg_pts_;
  int cur_itg_pt_;

  BasisValues values_;
};
