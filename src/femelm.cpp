#include <femelm.h>

#include <common/exceptions.h>
#include <basis/functions/box_linear.h>

static BoxLinearBasis box_linear_1d(1);
static BoxLinearBasis box_linear_2d(2);
static BoxLinearBasis box_linear_3d(3);

static std::vector<GaussPoint> box_linear_1d_gps = {
  std::make_pair(Eigen::Vector3d(-sqrt(1.0 / 3), 0.0, 0.0), 1.0),
  std::make_pair(Eigen::Vector3d(+sqrt(1.0 / 3), 0.0, 0.0), 1.0),
};

static std::vector<GaussPoint> box_linear_2d_gps = {
  std::make_pair(Eigen::Vector3d(-sqrt(1.0 / 3), -sqrt(1.0 / 3), 0.0), 1.0),
  std::make_pair(Eigen::Vector3d(+sqrt(1.0 / 3), -sqrt(1.0 / 3), 0.0), 1.0),
  std::make_pair(Eigen::Vector3d(+sqrt(1.0 / 3), +sqrt(1.0 / 3), 0.0), 1.0),
  std::make_pair(Eigen::Vector3d(-sqrt(1.0 / 3), +sqrt(1.0 / 3), 0.0), 1.0),
};

static std::vector<GaussPoint> box_linear_3d_gps = {
  std::make_pair(Eigen::Vector3d(-sqrt(1.0 / 3), -sqrt(1.0 / 3), -sqrt(1.0 / 3)), 1.0),
  std::make_pair(Eigen::Vector3d(+sqrt(1.0 / 3), -sqrt(1.0 / 3), -sqrt(1.0 / 3)), 1.0),
  std::make_pair(Eigen::Vector3d(+sqrt(1.0 / 3), +sqrt(1.0 / 3), -sqrt(1.0 / 3)), 1.0),
  std::make_pair(Eigen::Vector3d(-sqrt(1.0 / 3), +sqrt(1.0 / 3), -sqrt(1.0 / 3)), 1.0),
  std::make_pair(Eigen::Vector3d(-sqrt(1.0 / 3), -sqrt(1.0 / 3), +sqrt(1.0 / 3)), 1.0),
  std::make_pair(Eigen::Vector3d(+sqrt(1.0 / 3), -sqrt(1.0 / 3), +sqrt(1.0 / 3)), 1.0),
  std::make_pair(Eigen::Vector3d(+sqrt(1.0 / 3), +sqrt(1.0 / 3), +sqrt(1.0 / 3)), 1.0),
  std::make_pair(Eigen::Vector3d(-sqrt(1.0 / 3), +sqrt(1.0 / 3), +sqrt(1.0 / 3)), 1.0),
};


void FEMElm::refill(Mesh* mesh, ElemID elem_id)
{
  switch (mesh->elem_type(elem_id)) {
    case ELEM_LINE:
      bf_ = &box_linear_1d;
      itg_pts_ = &box_linear_1d_gps;
      break;
    case ELEM_QUAD:
      bf_ = &box_linear_2d;
      itg_pts_ = &box_linear_2d_gps;
      break;
    case ELEM_HEX:
      bf_ = &box_linear_3d;
      itg_pts_ = &box_linear_3d_gps;
      break;
    default: throw NotImplementedException() << "Basis function not implemented";
  }

  cur_itg_pt_ = -1;
  mesh_ = mesh;
  elem_id_ = elem_id;

  const auto& conn = mesh->elem_connectivity(elem_id);
  std::vector<Eigen::Vector3d> nodes(conn.size());
  for (unsigned int i = 0; i < nodes.size(); i++) {
    nodes[i] = mesh->node_pos(conn.at(i));
  }

  nodes_ = ElemNodes(std::move(nodes));
}

bool FEMElm::next_itg_pt()
{
  cur_itg_pt_++;
  if ((cur_itg_pt_ >= (int)itg_pts_->size()))
	  return false;

  const GaussPoint& gp = itg_pts_->at(cur_itg_pt_);
  bf_->calc_values(gp.first, gp.second, nodes_, BASIS_ALL, &values_);
  return true;
}
