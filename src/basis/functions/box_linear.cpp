#include <basis/functions/box_linear.h>

using namespace Eigen;

constexpr double epsilon_a[3][8] = {
  { -1, +1, +1, -1, -1, +1, +1, -1 },  // x
  { -1, -1, +1, +1, -1, -1, +1, +1 },  // y
  { -1, -1, -1, -1, +1, +1, +1, +1 }   // z
};

decltype(BasisValues::N) BoxLinearBasis::calc_N(const Vector3d& localPt) {
  decltype(BasisValues::N) n_out(nbf_);

  for (int bf = 0; bf < nbf_; bf++) {
    n_out(bf) = 1.0;

    if (nsd_ >= 1)  // 1D component
      n_out(bf) *= (1 + epsilon_a[0][bf] * localPt.x());

    if (nsd_ >= 2)  // 2D component
      n_out(bf) *= (1 + epsilon_a[1][bf] * localPt.y());

    if (nsd_ >= 3)  // 3D component
      n_out(bf) *= (1 + epsilon_a[2][bf] * localPt.z());

    n_out(bf) /= nbf_;
  }

  return n_out;
}

decltype(BasisValues::dNde) BoxLinearBasis::calc_dNde(const Vector3d& localPt) {
  decltype(BasisValues::dNde) dnde_out(nbf_, nsd_);

  for (int bf = 0; bf < nbf_; bf++) {
    for (int d = 0; d < nsd_; d++) {
      switch (nsd_) {
        case 1:
          dnde_out(bf, d) = epsilon_a[0][bf] / nbf_;
          break;
        case 2:
          switch (d) {  // 2D
            case 0:
              dnde_out(bf, d) = epsilon_a[0][bf] * (1 + epsilon_a[1][bf] * localPt.y()) / nbf_;
              break;
            case 1:
              dnde_out(bf, d) = (1 + epsilon_a[0][bf] * localPt.x()) * epsilon_a[1][bf] / nbf_;
              break;
          }
          break;
        case 3:
          switch (d) {  // 3D
            case 0:
              dnde_out(bf, d) = epsilon_a[0][bf] * (1 + epsilon_a[1][bf] * localPt.y()) * (1 + epsilon_a[2][bf] * localPt.z()) / nbf_;
              break;
            case 1:
              dnde_out(bf, d) = (1 + epsilon_a[0][bf] * localPt.x()) * epsilon_a[1][bf] * (1 + epsilon_a[2][bf] * localPt.z()) / nbf_;
              break;
            case 2:
              dnde_out(bf, d) = (1 + epsilon_a[0][bf] * localPt.x()) * (1 + epsilon_a[1][bf] * localPt.y()) * epsilon_a[2][bf] / nbf_;
              break;
          }
          break;
      }
    }
  }

  return dnde_out;
}
