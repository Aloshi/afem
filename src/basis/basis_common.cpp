#include <basis/basis_common.h>

void BasisCommon::calc_values(const decltype(BasisValues::itg_pt)& itg_pt, double weight, const ElemNodes& elem,
                              unsigned int flags, BasisValues* vals) {
  vals->itg_pt = itg_pt;

  // calculate N
  vals->N = calc_N(itg_pt);

  if (flags & BASIS_POSITION) {
    vals->position = calc_position(vals->N, elem);
  }

  if (flags & BASIS_FIRST_DERIVATIVE) {
    vals->dNde = calc_dNde(itg_pt);
    vals->dXde = calc_dXde(vals->dNde, elem);
    vals->cof = calc_cofactor(vals->dXde);
    vals->jacobian = vals->dXde.determinant();
    vals->jacc_x_weight = vals->jacobian * jacobian_scale_ * weight;
    vals->dN = calc_dN(vals->dNde, vals->cof, vals->jacobian);
  }
}

Eigen::Vector3d BasisCommon::calc_position(const decltype(BasisValues::N)& n, const ElemNodes& elem) {
  Eigen::Vector3d out(0.0, 0.0, 0.0);
  for (int a = 0; a < nbf_; a++) {
    out += elem.node_pos(a) * n(a);
  }
  return out;
}


decltype(BasisValues::dXde) BasisCommon::calc_dXde(const decltype(BasisValues::dNde)& dnde, const ElemNodes& elem) {
  Eigen::MatrixXd dxde_out(nsd_, nsd_);

  for (int i = 0; i < nsd_; i++) {
    for (int j = 0; j < nsd_; j++) {
      dxde_out(i, j) = 0;
      for (int a = 0; a < nbf_; a++) {
        const int elm_idx = a / nbf_per_node_;
        dxde_out(i, j) += dnde(a, j) * elem.node_pos(elm_idx)(i);
      }
    }
  }

  return dxde_out;
}

decltype(BasisValues::cof) BasisCommon::calc_cofactor(const decltype(BasisValues::dXde)& dxde) {
  if (nsd_ == 1)
    return Eigen::MatrixXd::Ones(1, 1);

  Eigen::MatrixXd cof(dxde.rows(), dxde.cols());
  cof = dxde.inverse().transpose() * dxde.determinant();

  return cof;
}


decltype(BasisValues::dN) BasisCommon::calc_dN(const decltype(BasisValues::dNde)& dnde, const decltype(BasisValues::cof)& cof, double jacc) {
  decltype(BasisValues::dN) dn_out(nbf_, nsd_);

  for (int a = 0; a < nbf_; a++) {
    for (int dir = 0; dir < nsd_; dir++) {
      dn_out(a, dir) = 0;

      // 1D
      dn_out(a, dir) += dnde(a, 0) * cof(dir, 0);

      if (nsd_ >= 2) {  // 2D
        dn_out(a, dir) += dnde(a, 1) * cof(dir, 1);
      }
      if (nsd_ >= 3) {  // 3D
        dn_out(a, dir) += dnde(a, 2) * cof(dir, 2);
      }

      dn_out(a, dir) /= jacc;
    }
  }

  return dn_out;
}
