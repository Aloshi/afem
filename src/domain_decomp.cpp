#include <mesh.h>
#include <mesh_data.h>
#include <node_variable.h>

#include <numeric>
#include <map>
#include <vector>
#include <memory>

#include <metis.h>
#include <parmetis.h>

#include <mesh_io/gmsh_ascii.h>

idx_t nodes_per_surface(ElemType t)
{
  switch (t) {
    case ELEM_LINE: return 1;
    case ELEM_QUAD: return 2;
    case ELEM_HEX: return 4;
  }

  throw NotImplementedException();
}

ElemType gmsh2afem(gmsh::ElementType t) {
  using namespace gmsh;
  switch (t) {
    case LINE_ORDER1: return ELEM_LINE;
    case BOX_ORDER1: return ELEM_QUAD;
    case HEXAHEDRON_ORDER1: return ELEM_HEX;
  }
  throw NotImplementedException();
}

// takes in each local process's (unique) elemental connectivity and returns
// a partitioning for it (which process should own which element) that
// minimizes communication (i.e. the number of shared nodes)
std::vector<idx_t> calc_elm_partition(const std::vector<GlobalNodeID>& conn_data,
    const std::vector<idx_t>& conn_offsets,
    const std::vector<ElemType>& elm_types
    ) {

  MPI_Comm comm = PETSC_COMM_WORLD;
  int rank, size;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &size);

  // gather how many elements each process has
  int n_lcl_elms = elm_types.size();
  std::vector<int> elmdist(size + 1);
  MPI_Allgather(&n_lcl_elms, 1, MPI_INT, elmdist.data(), 1, MPI_INT, comm);

  // this forms the "elmdist" array for ParMETIS and must be the same on each process
  // last entry is the total number of elements (sum of rest of array)
  // TODO this is a confusing way to do this (although it is in-place)
  for (int i = size; i >= 0; i--) {
    elmdist.at(i) = std::accumulate(elmdist.begin(), elmdist.end() - (size + 1 - i), 0);
  }

  idx_t min_nodes_per_surf_lcl = 9999;
  for (unsigned int i = 0; i < elm_types.size(); i++) {
    min_nodes_per_surf_lcl = std::min(min_nodes_per_surf_lcl, nodes_per_surface(elm_types.at(i)));
  }
  idx_t min_nodes_per_surf;
  MPI_Allreduce(&min_nodes_per_surf_lcl, &min_nodes_per_surf, 1, MPI_INT, MPI_MIN, comm);

  idx_t nparts = size;  // how many domains to decompose into (one for each process)
  idx_t wgtflag = 0;  // whether or not vertices have weights in their data (they don't)
  idx_t numflag = 0;  // number starts at 0 or 1
  idx_t ncon = 1;

  // minimum number of nodes in common between 2 elements before we add an edge to the graph
  // depends on number of nodes per surface, which depends on element types
  // ...which is unfortunately not necessary uniform for the whole mesh, so we use the minimum
  // number of nodes per surface and hope for the best
  idx_t ncommonnodes = min_nodes_per_surf;

  std::vector<real_t> tpwgts(ncon * nparts, 1.0 / nparts);
  std::vector<real_t> ubvec(ncon, 1.05);

  int options[10] = {};
  options[0] = 1;  // set to 1 for rest of options to take effect
  options[1] = 1;  // how much logging info to print
  options[2] = 99;  // seed

  // output parameters for partmeshkway
  idx_t edgecut;  
  std::vector<idx_t> partition(n_lcl_elms, -1);

  ParMETIS_V3_PartMeshKway(elmdist.data(), const_cast<idx_t*>(conn_offsets.data()), const_cast<idx_t*>(conn_data.data()), NULL, &wgtflag, &numflag,
                           &ncon, &ncommonnodes, &nparts, tpwgts.data(), ubvec.data(), options,
                           &edgecut, partition.data(), &comm);

  // partition now contains the processor each local element should belong to;
  // i.e. partition[3] == 2 means local element 3 should be owned by process 2
  return partition;
}

// mesh is not the same on every process, but may not be balanced
void balance_elements(
    std::vector<GlobalNodeID>& old_conn,
    std::vector<idx_t>& old_starts,
    std::vector<ElemType>& old_types) {
  int rank, size;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  MPI_Comm_size(PETSC_COMM_WORLD, &size);

  // first, find a partitioning for the elements
  std::vector<idx_t> partition = calc_elm_partition(old_conn, old_starts, old_types);

  // connectivity
  // connectivity is not so straight forward, because we allow for mixed
  // element types in the mesh, so varying numbers of of connectivity per elem

  // need to tell every process how much connectivity data it needs to hold
  std::vector<int> conn_sent_to_proc(size, 0), elms_sent_to_proc(size, 0);
  for (unsigned int i = 0; i < partition.size(); i++) {
    elms_sent_to_proc.at(partition.at(i)) += 1;

    idx_t conn_size = old_starts.at(i+1) - old_starts.at(i);
    conn_sent_to_proc.at(partition.at(i)) += conn_size;
  }

  // every process sends conn_sent_to_proc[i] to process i (same for elms_sent_to_proc[i])
  std::vector<int> n_elms_received(size), n_conn_received(size), n_conn_indices_received(size);
  for (int i = 0; i < size; i++) {
    // n_conn_received is only significant when i == rank, which is what we want
    MPI_Gather(&conn_sent_to_proc[i], 1, MPI_INT, n_conn_received.data(), 1, MPI_INT, i, PETSC_COMM_WORLD);
    MPI_Gather(&elms_sent_to_proc[i], 1, MPI_INT, n_elms_received.data(), 1, MPI_INT, i, PETSC_COMM_WORLD);
  }

  for (int i = 0; i < size; i++) {
    n_conn_indices_received.at(i) = n_elms_received.at(i) + 1;
  }

  int new_conn_size = std::accumulate(n_conn_received.begin(), n_conn_received.end(), 0);
  int new_lcl_elm_count = std::accumulate(n_elms_received.begin(), n_elms_received.end(), 0);

  std::vector<idx_t> conn_displs(size, 0), conn_idx_displs(size, 0), elm_displs(size, 0);
  for (int i = 1; i < size; i++) {
    conn_displs.at(i) = conn_displs.at(i - 1) + n_conn_received.at(i - 1);
    conn_idx_displs.at(i) = conn_idx_displs.at(i - 1) + n_elms_received.at(i - 1) + 1;
    elm_displs.at(i) = elm_displs.at(i - 1) + n_elms_received.at(i - 1);
  }

  std::vector<GlobalNodeID> new_conn(new_conn_size);
  std::vector<idx_t> new_conn_indices(new_lcl_elm_count + size);
  std::vector<ElemType> new_elm_types(new_lcl_elm_count);

  for (int send_to = 0; send_to < size; send_to++) {
    std::vector<GlobalNodeID> conn_for_proc;
    conn_for_proc.reserve(conn_sent_to_proc.at(send_to));

    std::vector<ElemType> elm_types_for_proc;
    elm_types_for_proc.reserve(elms_sent_to_proc.at(send_to));

    std::vector<idx_t> conn_idx_for_proc;
    conn_idx_for_proc.reserve(elms_sent_to_proc.at(send_to) + 1);

    idx_t offset = 0;
    for (unsigned int i = 0; i < partition.size(); i++) {
      if (partition.at(i) == send_to) {
        conn_idx_for_proc.push_back(offset);
        int conn_start = old_starts.at(i);
        int conn_end = old_starts.at(i+1);
        std::copy(old_conn.begin() + conn_start, old_conn.begin() + conn_end, std::back_inserter(conn_for_proc));
        elm_types_for_proc.push_back(old_types.at(i));
        offset += (conn_end - conn_start);
      }
    }
    conn_idx_for_proc.push_back(offset);

    assert(conn_for_proc.size() == conn_sent_to_proc.at(send_to));
    assert(conn_idx_for_proc.size() == elms_sent_to_proc.at(send_to) + 1);
    assert(elm_types_for_proc.size() == elms_sent_to_proc.at(send_to));

    MPI_Gatherv(conn_for_proc.data(), conn_for_proc.size(), MPI_INT,
                new_conn.data(), n_conn_received.data(), conn_displs.data(), MPI_INT,
                send_to, PETSC_COMM_WORLD);
    MPI_Gatherv(conn_idx_for_proc.data(), conn_idx_for_proc.size(), MPI_INT,
                new_conn_indices.data(), n_conn_indices_received.data(), conn_idx_displs.data(), MPI_INT,
                send_to, PETSC_COMM_WORLD);
    MPI_Gatherv(elm_types_for_proc.data(), elm_types_for_proc.size(), MPI_INT,
                new_elm_types.data(), n_elms_received.data(), elm_displs.data(), MPI_INT,
                send_to, PETSC_COMM_WORLD);
  }

  // fix up new_conn_indices to be properly contiguous - right now, each process's section starts at 0
  // (and now the last entry for each processor is weird - it should only exist in proc size-1's section of elements)
  idx_t offset = 0;
  for (int from_proc = 0; from_proc < size; from_proc++) {
    int n_elms = n_elms_received.at(from_proc);
    for (int i = 0; i < n_elms; i++) {
      int idx = conn_idx_displs.at(from_proc) + i;
      new_conn_indices.at(idx) += offset;
    }

    if (from_proc == size - 1)
      new_conn_indices.at(conn_idx_displs.at(from_proc) + n_elms) += offset;
    else
      offset += new_conn_indices.at(conn_idx_displs.at(from_proc) + n_elms);
  }

  // eliminate new_conn_indices.at(conn_idx_displs + n_elms - rank) for each procs 0..size-2 as it is not a valid element anymore
  for (int from_proc = 0; from_proc < size - 1; from_proc++) {
    new_conn_indices.erase(new_conn_indices.begin() + conn_idx_displs.at(from_proc) + n_elms_received.at(from_proc) - from_proc);
  }

  assert(new_conn_indices.size() == new_lcl_elm_count + 1);

  old_conn = std::move(new_conn);
  old_starts = std::move(new_conn_indices);
  old_types = std::move(new_elm_types);

  // elements have been balanced
}

// the connectivity data that is used in balance_elements is all global node IDs
// we need to convert these global node IDs into a set of local node IDs, and
// keep a mapping of local ID back to global ID (to keep track of where nodes
// go in the global matrix/vector)
// this returns two things: lcl_to_glb_out, which is a mapping of
// local node ID -> global node ID
// (i.e. lcl_to_glb[i] == j means local node i corresponds to global node j)
void build_local_to_global(const std::vector<GlobalNodeID>& conn_data,
    const std::vector<idx_t>& conn_indices,
    std::vector<GlobalNodeID>* lcl_to_glb_out,
    std::vector<LocalNodeID>* lcl_conn_data_out) {

  lcl_to_glb_out->clear();
  lcl_conn_data_out->clear();

  std::map<GlobalNodeID, LocalNodeID> seen_nodes;
  for (unsigned int elem = 0; elem < conn_indices.size() - 1; elem++) {
    for (unsigned int j = conn_indices.at(elem); j < conn_indices.at(elem + 1); j++) {
      GlobalNodeID glb_node_idx = conn_data.at(j);
      auto it = seen_nodes.find(glb_node_idx);
      if (it == seen_nodes.end()) {
        LocalNodeID lcl_idx = lcl_to_glb_out->size();
        seen_nodes.insert({glb_node_idx, lcl_idx});
        lcl_to_glb_out->push_back(glb_node_idx);
        lcl_conn_data_out->push_back(lcl_idx);
      } else {
        lcl_conn_data_out->push_back(it->second);
      }
    }
  }

  assert(lcl_conn_data_out->size() == conn_data.size());
  assert(seen_nodes.size() == lcl_to_glb_out->size());
}

std::shared_ptr<Mesh> load_gmsh_distributed(const std::string& path) {
  int rank, size;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  MPI_Comm_size(PETSC_COMM_WORLD, &size);

  GlobalNodeID n_glb_nodes;
  ElemID n_glb_elems;
  std::vector<GlobalNodeID> my_conn;
  std::vector<idx_t> my_conn_indices;
  std::vector<ElemType> my_elem_types;

  // master-slave element loading for ASCII Gmsh files
  if (rank == 0) {
    gmsh::Reader r;
    r.open(path);

    // tell everyone the size of the global system
    n_glb_nodes = r.n_nodes();
    MPI_Bcast(&n_glb_nodes, 1, MPI_INT, 0, PETSC_COMM_WORLD);
    n_glb_elems = r.elm_type_counts().at(r.primary_elm_type());
    MPI_Bcast(&n_glb_elems, 1, MPI_INT, 0, PETSC_COMM_WORLD);

    // send element data to each process (and ourself)
    for (unsigned int proc = 0; proc < size; proc++) {
      int elms_this_proc = n_glb_elems / size + (proc < n_glb_elems % size ? 1 : 0);

      std::vector<GlobalNodeID> proc_conn;
      std::vector<idx_t> proc_conn_indices;
      std::vector<ElemType> proc_elem_types;

      // try to reserve memory ahead of time for
      // better performance during push_back
      proc_conn_indices.reserve(elms_this_proc + 1);
      proc_elem_types.reserve(elms_this_proc);

      // read data from the file to populate the vectors above
      idx_t offset = 0;
      for (ElemID elem_idx = 0; elem_idx < elms_this_proc; elem_idx++) {
        gmsh::Element elem = r.read_element();

        // skip if this isn't a "real" element
        if (elem.type != r.primary_elm_type()) {
          elem_idx--;
          continue;
        }

        // copy connectivity into proc_conn and make it 0-indexed
        for (unsigned int i = 0; i < elem.connectivity.size(); i++) {
          proc_conn.push_back(elem.connectivity.at(i) - 1);  // gmsh files are 1-indexed, so -1 here
        }

        proc_conn_indices.push_back(offset);
        proc_elem_types.push_back(gmsh2afem(elem.type));

        offset += elem.connectivity.size();
      }
      proc_conn_indices.push_back(offset);

      // if this is data for proc0, skip the MPI_Send and just move the
      // data into the my_* vectors directly
      if (proc == rank) {
        my_conn = std::move(proc_conn);
        my_conn_indices = std::move(proc_conn_indices);
        my_elem_types = std::move(proc_elem_types);
      } else {
        int data[2] = {
          static_cast<int>(proc_conn.size()),
          static_cast<int>(proc_elem_types.size())
        };
        MPI_Send(data, 2, MPI_INT, proc, 0, PETSC_COMM_WORLD);

        MPI_Send(proc_conn.data(), proc_conn.size(), MPI_INT, proc, 0, PETSC_COMM_WORLD);
        MPI_Send(proc_conn_indices.data(), proc_conn_indices.size(), MPI_INT, proc, 0, PETSC_COMM_WORLD);
        MPI_Send(proc_elem_types.data(), proc_elem_types.size(), MPI_INT, proc, 0, PETSC_COMM_WORLD);
      }
    }

    r.close();
  } else {
    // receive info about the size of the global system
    // (needed later on for Mesh constructor)
    MPI_Bcast(&n_glb_nodes, 1, MPI_INT, 0, PETSC_COMM_WORLD);
    MPI_Bcast(&n_glb_elems, 1, MPI_INT, 0, PETSC_COMM_WORLD);

    // receive sizes for local arrays (my_* vectors)
    // data[0] = conn.size(), data[1] = elem_types.size()
    int data[2];
    MPI_Recv(data, 2, MPI_INT, 0, 0, PETSC_COMM_WORLD, MPI_STATUS_IGNORE);

    // resize our buffers according to the size information we just recv'd
    my_conn.resize(data[0]);
    my_conn_indices.resize(data[1] + 1);
    my_elem_types.resize(data[1]);

    // receive the data to fill those arrays
    MPI_Recv(my_conn.data(), my_conn.size(), MPI_INT, 0, 0, PETSC_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(my_conn_indices.data(), my_conn_indices.size(), MPI_INT, 0, 0, PETSC_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(my_elem_types.data(), my_elem_types.size(), MPI_INT, 0, 0, PETSC_COMM_WORLD, MPI_STATUS_IGNORE);
  }

  // balance element distribution using ParMETIS and MPI
  // (will modify the input arguments)
  balance_elements(my_conn, my_conn_indices, my_elem_types);

  // convert element connectivity from all global indices to local indices
  std::vector<GlobalNodeID> lcl_to_glb_node;
  std::vector<LocalNodeID> lcl_conn;
  build_local_to_global(my_conn, my_conn_indices, &lcl_to_glb_node, &lcl_conn);

  // load node data (each process does this individually...sketchy)

  // first need the inverse of lcl_to_glb_node to see where global IDs should map
  std::map<GlobalNodeID, LocalNodeID> glb_to_lcl;
  for (unsigned int i = 0; i < lcl_to_glb_node.size(); i++) {
    glb_to_lcl[lcl_to_glb_node[i]] = i;
  }

  using namespace Eigen;
  std::vector<Vector3d> node_positions(lcl_to_glb_node.size(), Vector3d(-1.0, -1.0, -1.0));

  gmsh::Reader r;
  r.open(path);

  // get node data by going through the entire file and only keeping
  // what we actually need for our local nodes
  for (unsigned int i = 0; i < r.n_nodes(); i++) {
    gmsh::Node node = r.read_node();
    int glb_node_idx = node.number - 1;  // Gmsh files are 1-indexed, so -1 here

    auto it = glb_to_lcl.find(glb_node_idx);
    if (it != glb_to_lcl.end()) {
      node_positions.at(it->second) = Vector3d(node.coords[0], node.coords[1], node.coords[2]);
    }
  }

  r.close();

  // un-flatten connectivity data back into nested vector format for Mesh to use
  std::vector< std::vector<LocalNodeID> > conn(my_elem_types.size());
  for (unsigned int i = 0; i < my_elem_types.size(); i++) {
    auto start = lcl_conn.begin() + my_conn_indices.at(i);
    auto end = lcl_conn.begin() + my_conn_indices.at(i+1);
    std::copy(start, end, std::back_inserter(conn.at(i)));
  }

  return std::shared_ptr<Mesh>(new Mesh(
      std::move(node_positions), std::vector<SurfaceIndicators>() /* TODO */,
      std::move(conn), std::move(my_elem_types), std::move(lcl_to_glb_node), std::vector<ElemID>() /* TODO */,
      n_glb_nodes, n_glb_elems));
}
