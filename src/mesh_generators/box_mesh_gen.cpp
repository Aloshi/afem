#include <mesh_generators/box_mesh_gen.h>
#include <common/log.h>
#include <common/exceptions.h>

BoxMeshGen::BoxMeshGen(unsigned int nsd, unsigned int n_elms_x, unsigned int n_elms_y, unsigned int n_elms_z)
  : nsd_(nsd), n_elms_ {n_elms_x, n_elms_y, n_elms_z}
{
  assert(nsd >= 1 && nsd <= 3);

  if (nsd < 2)
    n_elms_[1] = 0;
  if (nsd < 3)
    n_elms_[2] = 0;
}

std::shared_ptr<Mesh> BoxMeshGen::generate()
{
  ElemType type;
  switch (nsd_) {
    case 1: type = ELEM_LINE; break;
    case 2: type = ELEM_QUAD; break;
    case 3: type = ELEM_HEX; break;
    default: throw AFEMException() << "Invalid nsd '" << nsd_ << "'";
  }

  unsigned int nodes_per_elem = (unsigned int) pow(2, nsd_);

  unsigned int n_total_nodes = n_elms_[0] + 1;
  if (nsd_ >= 2)
    n_total_nodes *= n_elms_[1] + 1;
  if (nsd_ >= 3)
    n_total_nodes *= n_elms_[2] + 1;

  unsigned int n_total_elms = n_elms_[0];
  if (nsd_ >= 2)
    n_total_elms *= n_elms_[1];
  if (nsd_ >= 3)
    n_total_elms *= n_elms_[2];

  // create nodes
  std::vector<Vector3> pos(n_total_nodes);
  std::vector<SurfaceIndicators> surf(n_total_nodes);
  for (unsigned int z_idx = 0; z_idx <= n_elms_[2]; z_idx++) {
    for (unsigned int y_idx = 0; y_idx <= n_elms_[1]; y_idx++) {
      for (unsigned int x_idx = 0; x_idx <= n_elms_[0]; x_idx++) {
        double x = x_idx / (double) n_elms_[0];
        double y = nsd_ >= 2 ? (y_idx / (double) n_elms_[1]) : 0.0;
        double z = nsd_ >= 3 ? (z_idx / (double) n_elms_[2]) : 0.0;

        unsigned int node_idx = ((n_elms_[0] + 1) * (n_elms_[1] + 1)) * z_idx + (n_elms_[0] + 1) * y_idx + x_idx;
        pos[node_idx] << x, y, z;

        surf[node_idx] = 0;
        if (x_idx == 0)
          surf[node_idx] |= (1 << 0);
        else if (x_idx == n_elms_[0])
          surf[node_idx] |= (1 << 1);

        if (y_idx == 0 && nsd_ >= 2)
          surf[node_idx] |= (1 << 2);
        else if (y_idx == n_elms_[1] && nsd_ >= 2)
          surf[node_idx] |= (1 << 3);

        if (z_idx == 0 && nsd_ >= 3)
          surf[node_idx] |= (1 << 4);
        else if (z_idx == n_elms_[2] && nsd_ >= 3)
          surf[node_idx] |= (1 << 5);
      }
    }
  }

  std::vector< std::vector<LocalNodeID> > conn(n_total_elms);
  for (unsigned int z_idx = 0; z_idx < (nsd_ >= 3 ? n_elms_[2] : 1); z_idx++) {
    for (unsigned int y_idx = 0; y_idx < (nsd_ >= 2 ? n_elms_[1] : 1); y_idx++) {
      for (unsigned int x_idx = 0; x_idx < n_elms_[0]; x_idx++) {
        unsigned int elem_idx = (n_elms_[0] * n_elms_[1]) * z_idx + n_elms_[0] * y_idx + x_idx;

        if (nsd_ == 1) {
          conn[elem_idx] = {
            node_index(x_idx),
            node_index(x_idx + 1),
          };
        } else if (nsd_ == 2) {
          conn[elem_idx] = {
            node_index(x_idx, y_idx),
            node_index(x_idx+1, y_idx),
            node_index(x_idx+1, y_idx+1),
            node_index(x_idx, y_idx+1),
          };
        } else if (nsd_ == 3) {
          conn[elem_idx] = {
            node_index(x_idx, y_idx, z_idx),
            node_index(x_idx+1, y_idx, z_idx),
            node_index(x_idx+1, y_idx+1, z_idx),
            node_index(x_idx, y_idx+1, z_idx),

            node_index(x_idx, y_idx, z_idx+1),
            node_index(x_idx+1, y_idx, z_idx+1),
            node_index(x_idx+1, y_idx+1, z_idx+1),
            node_index(x_idx, y_idx+1, z_idx+1),
          };
        } else {
          throw std::runtime_error("Invalid nsd");
        }

        assert(conn[elem_idx].size() == nodes_per_elem);
      }
    }
  }

  std::vector<ElemType> types;
  types.resize(n_total_elms, type);

  return std::make_shared<Mesh>(std::move(pos), std::move(surf), std::move(conn), std::move(types));
}

std::shared_ptr<Mesh> BoxMeshGen::generate_distributed()
{
  ElemType type;
  switch (nsd_) {
    case 1: type = ELEM_LINE; break;
    case 2: type = ELEM_QUAD; break;
    case 3: type = ELEM_HEX; break;
    default: throw AFEMException() << "Invalid nsd '" << nsd_ << "'";
  }

  int rank, size;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  MPI_Comm_size(PETSC_COMM_WORLD, &size);

  unsigned int nodes_per_elem = (unsigned int) pow(2, nsd_);

  unsigned int n_total_glb_nodes = n_elms_[0] + 1;
  if (nsd_ >= 2)
    n_total_glb_nodes *= n_elms_[1] + 1;
  if (nsd_ >= 3)
    n_total_glb_nodes *= n_elms_[2] + 1;

  unsigned int n_total_glb_elms = n_elms_[0];
  if (nsd_ >= 2)
    n_total_glb_elms *= n_elms_[1];
  if (nsd_ >= 3)
    n_total_glb_elms *= n_elms_[2];

  // calc distribution of elems/nodes
  unsigned int elm_offset[3] {};
  unsigned int n_lcl_elms[3] {};
  unsigned int n_lcl_nodes[3] {};
  unsigned int n_glb_nodes[3] {};

  for (int dim = 0; dim < nsd_; dim++) {
    n_glb_nodes[dim] = (n_elms_[dim] + 1);

    if (dim == 0) {
      unsigned int chunk_size = (n_elms_[dim] / size);
      if (rank == size - 1)
        chunk_size += n_elms_[dim] % size;

      n_lcl_elms[dim] = chunk_size;
      elm_offset[dim] = (n_elms_[dim] / size) * rank;
    } else {
      n_lcl_elms[dim] = n_elms_[dim];
      elm_offset[dim] = 0;
    }

    n_lcl_nodes[dim] = n_lcl_elms[dim] + 1;
  }

  unsigned int n_total_lcl_nodes = n_lcl_nodes[0];
  if (nsd_ >= 2)
    n_total_lcl_nodes *= n_lcl_nodes[1];
  if (nsd_ >= 3)
    n_total_lcl_nodes *= n_lcl_nodes[2];

  unsigned int n_total_lcl_elms = n_lcl_elms[0];
  if (nsd_ >= 2)
    n_total_lcl_elms *= n_lcl_elms[1];
  if (nsd_ >= 3)
    n_total_lcl_elms *= n_lcl_elms[2];

  //PetscSynchronizedPrintf(PETSC_COMM_WORLD, "elm_offset = {%u, %u, %u}, n_lcl_elms = {%u, %u, %u}\n", elm_offset[0], elm_offset[1], elm_offset[2], n_lcl_elms[0], n_lcl_elms[1], n_lcl_elms[2]);

  // create nodes
  std::vector<Vector3> pos(n_total_lcl_nodes);
  std::vector<SurfaceIndicators> surf(n_total_lcl_nodes);
  std::vector<GlobalNodeID> local_to_global_node(n_total_lcl_nodes);
  for (unsigned int z_idx = 0; z_idx <= n_lcl_elms[2]; z_idx++) {
    for (unsigned int y_idx = 0; y_idx <= n_lcl_elms[1]; y_idx++) {
      for (unsigned int x_idx = 0; x_idx <= n_lcl_elms[0]; x_idx++) {
        unsigned int x_idx_glb = x_idx + elm_offset[0];
        unsigned int y_idx_glb = y_idx + elm_offset[1];
        unsigned int z_idx_glb = z_idx + elm_offset[2];

        double x = x_idx_glb / (double) n_elms_[0];
        double y = nsd_ >= 2 ? (y_idx_glb / (double) n_elms_[1]) : 0.0;
        double z = nsd_ >= 3 ? (z_idx_glb / (double) n_elms_[2]) : 0.0;

        LocalNodeID lcl_node_idx = n_lcl_nodes[0] * n_lcl_nodes[1] * z_idx + n_lcl_nodes[0] * y_idx + x_idx;
        unsigned int glb_node_idx = n_glb_nodes[0] * n_glb_nodes[1] * z_idx_glb + n_glb_nodes[0] * y_idx_glb + x_idx_glb;
        pos.at(lcl_node_idx) << x, y, z;
        local_to_global_node.at(lcl_node_idx) = glb_node_idx;

        //PetscSynchronizedPrintf(PETSC_COMM_WORLD, "x_idx %u, y_idx %u, z_idx %u     ", x_idx, y_idx, z_idx);
        //PetscSynchronizedPrintf(PETSC_COMM_WORLD, "x_idx_glb %u, y_idx_glb %u, z_idx_glb %u\n", x_idx_glb, y_idx_glb, z_idx_glb);
        //PetscSynchronizedPrintf(PETSC_COMM_WORLD, "r%d, node %u (%lf, %lf, %lf) maps to %u\n", rank, lcl_node_idx, x, y, z, glb_node_idx);

        surf[lcl_node_idx] = 0;
        if (x_idx_glb == 0)
          surf[lcl_node_idx] |= (1 << 0);
        else if (x_idx_glb == n_elms_[0])
          surf[lcl_node_idx] |= (1 << 1);

        if (y_idx_glb == 0 && nsd_ >= 2)
          surf[lcl_node_idx] |= (1 << 2);
        else if (y_idx_glb == n_elms_[1] && nsd_ >= 2)
          surf[lcl_node_idx] |= (1 << 3);

        if (z_idx_glb == 0 && nsd_ >= 3)
          surf[lcl_node_idx] |= (1 << 4);
        else if (z_idx_glb == n_elms_[2] && nsd_ >= 3)
          surf[lcl_node_idx] |= (1 << 5);
      }
    }
  }
  PetscSynchronizedFlush(PETSC_COMM_WORLD, stdout);

  auto lcl_node_index = [&n_lcl_nodes] (unsigned int x, unsigned int y, unsigned int z) {
    return z * (n_lcl_nodes[0] * n_lcl_nodes[1]) + y * n_lcl_nodes[0] + x;
  };

  std::vector< std::vector<LocalNodeID> > conn(n_total_lcl_elms);
  std::vector<ElemID> local_to_global_elem(n_total_lcl_nodes);
  for (unsigned int z_idx = 0; z_idx < (nsd_ >= 3 ? n_lcl_elms[2] : 1); z_idx++) {
    for (unsigned int y_idx = 0; y_idx < (nsd_ >= 2 ? n_lcl_elms[1] : 1); y_idx++) {
      for (unsigned int x_idx = 0; x_idx < n_lcl_elms[0]; x_idx++) {

        unsigned int x_idx_glb = x_idx + elm_offset[0];
        unsigned int y_idx_glb = y_idx + elm_offset[1];
        unsigned int z_idx_glb = z_idx + elm_offset[2];

        unsigned int glb_elem_idx = (n_elms_[0] * n_elms_[1]) * z_idx_glb + n_elms_[0] * y_idx_glb + x_idx_glb;
        unsigned int lcl_elem_idx = (n_lcl_elms[0] * n_lcl_elms[1]) * z_idx + n_lcl_elms[0] * y_idx + x_idx;
        local_to_global_elem.at(lcl_elem_idx) = glb_elem_idx;

        //PetscSynchronizedPrintf(PETSC_COMM_WORLD, "r%d, elem %u maps to %u\n", rank, lcl_elem_idx, glb_elem_idx);

        if (nsd_ == 1) {
          conn[lcl_elem_idx] = {
            lcl_node_index(x_idx, 0, 0),
            lcl_node_index(x_idx + 1, 0, 0),
          };
        } else if (nsd_ == 2) {
          conn[lcl_elem_idx] = {
            lcl_node_index(x_idx, y_idx, 0),
            lcl_node_index(x_idx+1, y_idx, 0),
            lcl_node_index(x_idx+1, y_idx+1, 0),
            lcl_node_index(x_idx, y_idx+1, 0),
          };
        } else if (nsd_ == 3) {
          conn[lcl_elem_idx] = {
            lcl_node_index(x_idx, y_idx, z_idx),
            lcl_node_index(x_idx+1, y_idx, z_idx),
            lcl_node_index(x_idx+1, y_idx+1, z_idx),
            lcl_node_index(x_idx, y_idx+1, z_idx),

            lcl_node_index(x_idx, y_idx, z_idx+1),
            lcl_node_index(x_idx+1, y_idx, z_idx+1),
            lcl_node_index(x_idx+1, y_idx+1, z_idx+1),
            lcl_node_index(x_idx, y_idx+1, z_idx+1),
          };
        } else {
          throw std::runtime_error("Invalid nsd");
        }

        assert(conn[lcl_elem_idx].size() == nodes_per_elem);
      }
    }
  }

  PetscSynchronizedFlush(PETSC_COMM_WORLD, stdout);

  std::vector<ElemType> types;
  types.resize(n_total_lcl_elms, type);

  return std::make_shared<Mesh>(std::move(pos), std::move(surf), std::move(conn), std::move(types),
      std::move(local_to_global_node), std::move(local_to_global_elem), n_total_glb_nodes, n_total_glb_elms);
}
