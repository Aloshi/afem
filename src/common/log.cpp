#include <common/log.h>
#include <stdio.h>
#include <assert.h>
#include <iostream>

#include <mpi.h>

FILE* Log::sFile = NULL;
int Log::sRank = -1;

std::string Log::getLogPath()
{
	return "log.txt";
}

void Log::open()
{
  MPI_Comm_rank(MPI_COMM_WORLD, &sRank);
  assert(sRank != -1);

  if (sRank == 0) {
    assert(sFile == NULL);
    sFile = fopen(getLogPath().c_str(), "w");
  }
}

std::ostringstream& Log::getStream(LogLevel level)
{
	/*switch (level) {
		case LogError:
			mStream << "ERROR: \t";
			break;
		case LogWarning:
			mStream << "WARNING: \t";
			break;
		default:
			break;
	}*/

	mMsgLevel = level;

	return mStream;
}

void Log::flush()
{
	fflush(sFile);
}

void Log::close()
{
	fclose(sFile);
	sFile = NULL;
}

Log::~Log()
{
  if (sRank != 0)
    return;

	mStream << std::endl;
	
	if(sFile == NULL)
	{
		// not open yet, print to stdout
		std::cerr << "ERROR - tried to write to log file before it was open! The following won't be logged:\n";
		std::cerr << mStream.str();
		return;
	}

	// write to log file
	fprintf(sFile, "%s", mStream.str().c_str());

	// also print to system console
	fprintf(stderr, "%s", mStream.str().c_str());
}
