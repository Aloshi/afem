#include <mesh_io/plt_writer.h>

#include <mesh_io/TecplotIO_ascii.h>

void PLTWriter::write(Mesh* mesh, void* node_data, size_t node_data_stride, const std::vector<NodeVariable>& variables, const std::string& path)
{
	const int nsd = 3;

  //if (mesh->is_distributed())
  //  throw NotImplementedException();

	w_.open(path.c_str(), false);

	TecplotHeader header;
	header.title = "title_here";
	header.variables = { "x" };
	if (nsd >= 2)
		header.variables.push_back("y");
	if (nsd >= 3)
		header.variables.push_back("z");
	for (auto it = variables.begin(); it != variables.end(); it++) {
		header.variables.push_back(it->name);
	}
	w_.write_header(header);

	TecplotZone zone;
	zone.format = kFiniteElementPoint;
	zone.elem_type = mesh->elem_type(0);
	zone.num_nodes = mesh->n_lcl_nodes();
	zone.num_elements = mesh->n_lcl_elements();
  zone.time = 0;
	w_.write_zone(zone);

  write_data(zone, mesh, node_data, node_data_stride, variables);
  w_.close();
}

void PLTWriter::write_timestep(double t, Mesh* mesh, void* node_data, size_t node_data_stride, const std::vector<NodeVariable>& variables, const std::string& path)
{
	const int nsd = 3;

  //if (mesh->is_distributed())
  //  throw NotImplementedException();

	w_.open(path.c_str(), true);

	TecplotZone zone;
	zone.format = kFiniteElementPoint;
	zone.elem_type = mesh->elem_type(0);
	zone.num_nodes = mesh->n_lcl_nodes();
	zone.num_elements = mesh->n_lcl_elements();
  zone.time = t;
	w_.write_zone(zone);

  write_data(zone, mesh, node_data, node_data_stride, variables);
  w_.close();
}

void PLTWriter::write_data(TecplotZone& zone, Mesh* mesh, void* node_data, size_t node_data_stride, const std::vector<NodeVariable>& variables) {
  const int nsd = 3;

	{
		std::vector<double> data(nsd + variables.size());
		for (LocalNodeID i = 0; i < mesh->n_lcl_nodes(); i++) {
			const auto& pos = mesh->node_pos(i);
			data[0] = pos.x();
			data[1] = pos.y();
			data[2] = pos.z();

			for (unsigned int v = 0; v < variables.size(); v++) {
				size_t off = i * node_data_stride + variables.at(v).offset;
				data[nsd + v] = *((double*)((char*)node_data + off));
			}

			w_.write_node(data.data());
		}
	}

	{
		std::vector<unsigned int> conn(zone.get_nodes_in_element());
		for (ElemID i = 0; i < mesh->n_lcl_elements(); i++) {
			const auto& elem = mesh->elem_connectivity(i);
			for (unsigned int j = 0; j < elem.size(); j++) {
				conn[j] = elem.at(j) + 1;
			}
			w_.write_elem(conn.data());
		}
	}
}
